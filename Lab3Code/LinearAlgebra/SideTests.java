//Mohammad Khan, 2038700
package LinearAlgebra;

public class SideTests
{
    public static void main(String[] args)
    {
        Vector3d vec1 = new Vector3d(1, 1, 2);
        Vector3d vec2 = new Vector3d(2, 3, 4);
        System.out.println(vec1.dotProduct(vec2));
        Vector3d vec3 = vec1.add(vec2);
        System.out.println(vec3.getXAxis());
        System.out.println(vec3.getYAxis());
        System.out.println(vec3.getZAxis());
    }
}
