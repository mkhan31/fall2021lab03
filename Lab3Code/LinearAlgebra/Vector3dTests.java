//Mohammad Khan, 2038700
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests
{
    @Test
    public void vectorTest()
    {
        Vector3d vecTest = new Vector3d(4, 7, 9);
        assertEquals(4, vecTest.getXAxis());
        assertEquals(7, vecTest.getYAxis());
        assertEquals(9, vecTest.getZAxis());
    }

    @Test
    public void magnitudeTest()
    {
        Vector3d vecMagnitude = new Vector3d(3, 4, 5);
        assertEquals(Math.sqrt(50), vecMagnitude.magnitude());
    }

    @Test
    public void dotTest()
    {
        Vector3d dot1 = new Vector3d(6, 9, 2);
        Vector3d dot2 = new Vector3d(2, 3, 4);
        assertEquals(47, dot1.dotProduct(dot2));
    }

    @Test
    public void addTest()
    {
        Vector3d add1 = new Vector3d(4, 6, 8);
        Vector3d add2 = new Vector3d(5, 7, 9);
        Vector3d addResult = add1.add(add2);
        assertEquals(9, addResult.getXAxis());
        assertEquals(13, addResult.getYAxis());
        assertEquals(17, addResult.getZAxis());
    }
}