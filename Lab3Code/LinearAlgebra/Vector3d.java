//Mohammad Khan, 2038700
package LinearAlgebra;

public class Vector3d
{
    private double x;
    private double y;
    private double z;

    public Vector3d(double xAxis, double yAxis, double zAxis)
    {
        this.x = xAxis;
        this.y = yAxis;
        this.z = zAxis;
    }

    public double getXAxis()
    {
        return this.x;
    }

    public double getYAxis()
    {
        return this.y;
    }

    public double getZAxis()
    {
        return this.z;
    }

    public double magnitude()
    {
        double mag = Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
        return mag;
    }

    public double dotProduct(Vector3d vec2)
    {
        double dotResult = (this.x*vec2.x) + (this.y*vec2.y) + (this.z*vec2.z);
        return dotResult;
    }

    public Vector3d add(Vector3d vec2)
    {
        Vector3d vec3 = new Vector3d((this.x + vec2.x), (this.y + vec2.y), (this.z + vec2.z));
        return vec3;
    }
}